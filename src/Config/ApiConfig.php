<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 16:00
 */

namespace Payone\Config;



class ApiConfig {

	/**
	 * your Portal Secret
	 * @see Portal | Extended | Key
	 * @var string
	 */
	private $portal_secret;

	/**
	 * The API Version to be used
	 * @see ApiConfig::API_VERSION
	 *
	 * @var string
	 */
	private $api_version = Globals::API_VERSION;

	/**
	 * The default encoding to be used
	 * @see ApiConfig::DEFAULT_ENCODING
	 * @var string
	 */
	private $encoding = Globals::DEFAULT_ENCODING;

	/**
	 * ApiConfig constructor.
	 *
	 * @param string $portal_secret
	 */
	public function __construct( $portal_secret ) {
		$this->portal_secret = $portal_secret;
	}

	public function getPortalSecret() {
		return $this->portal_secret;
	}

	public function setPortalSecret( $portal_secret ) {
		$this->portal_secret = $portal_secret;

		return $this;
	}

	public function getApiVersion() {
		return $this->api_version;
	}

	public function setApiVersion( $api_version ) {
		$this->api_version = $api_version;

		if ( ! $this->api_version ) {
			$this->api_version = Globals::API_VERSION;
		}

		return $this;
	}

	public function getEncoding() {
		return $this->encoding;
	}

	public function setEncoding( $encoding ) {
		$this->encoding = $encoding;

		if ( ! $this->encoding ) {
			$this->encoding = Globals::DEFAULT_ENCODING;
		}

		return $this;
	}

	/**
	 * returns the current secret hashed by the defined hash function
	 *
	 * @return string
	 */
	public function getSecretHash() {
		return hash("md5", $this->getPortalSecret() );
	}

	/**
	 * Magic method to receive the configuration data from this object as array (required by endpoint)
	 *
	 * @return array|boolean the array sorted by keys or false if no portalsecret has been defined
	 */
	public function __toArray( ) {

		if ( ! $this->getPortalSecret() ) {
			return false;
		}

		$self = [
			"key" => $this->getSecretHash(),
			"api_version" => $this->getApiVersion(),
			"encoding" => $this->getEncoding()
		];

		ksort( $self );

		return $self;
	}

}