<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 15:57
 */

namespace Payone\Config;


class PersonalDataConfig {

	/**
	 * @var string
	 */
	private $salutation;

	/**
	 * @var string
	 */
	private $firstname;

	/**
	 * @var string
	 */
	private $lastname;

	/**
	 * @var string
	 */
	private $street;

	/**
	 * @var string
	 */
	private $zip;

	/**
	 * @var string
	 */
	private $city;

	/**
	 * @var string
	 */
	private $country;

	/**
	 * @var string
	 */
	private $email;

	/**
	 * @var \DateTime
	 */
	private $birthday;

	/**
	 * @var string
	 */
	private $language;

	/**
	 * @var array
	 */
	private $additional_data;

	public function getSalutation() {
		return $this->salutation;
	}

	public function setSalutation( $salutation ) {
		$this->salutation = $salutation;

		return $this;
	}

	public function getFirstname() {
		return $this->firstname;
	}

	public function setFirstname( $firstname ) {
		$this->firstname = $firstname;

		return $this;
	}

	public function getLastname() {
		return $this->lastname;
	}

	public function setLastname( $lastname ) {
		$this->lastname = $lastname;

		return $this;
	}

	public function getStreet() {
		return $this->street;
	}

	public function setStreet( $street ) {
		$this->street = $street;

		return $this;
	}

	public function getZip() {
		return $this->zip;
	}

	public function setZip( $zip ) {
		$this->zip = $zip;

		return $this;
	}

	public function getCountry() {
		return $this->country;
	}

	public function setCountry( $country ) {
		$this->country = $country;

		return $this;
	}

	public function getEmail() {
		return $this->email;
	}

	public function setEmail( $email ) {
		$this->email = $email;

		return $this;
	}

	public function getBirthday() {
		return $this->birthday;
	}

	public function setBirthday( $birthday ) {
		$this->birthday = $birthday;

		return $this;
	}

	public function getLanguage() {
		return $this->language;
	}

	public function setLanguage( $language ) {
		$this->language = $language;

		return $this;
	}

	public function getCity() {
		return $this->city;
	}

	public function setCity( $city ) {
		$this->city = $city;

		return $this;
	}

	public function getAdditionalData() {
		return $this->additional_data;
	}

	public function setAdditionalData( $additional_data ) {
		$this->additional_data = $additional_data;

		return $this;
	}

	public function __toArray( ) {

		$date = null;

		if ( ! $this->getLastname() ) {
			throw new \Exception('Lastname must not be empty!');
		}

		if ( ! $this->getCountry() ) {
			throw new \Exception('Country must not be empty!');
		}

		if ( $this->getBirthday() ) {
			$date = $this->getBirthday()->format('Ymd');
		}

		if ( ! $this->additional_data ) {
			$this->additional_data = [];
		}

		$self = array_merge([
			"salutation" => $this->getSalutation(),
			"firstname" => $this->getFirstname(),
			"lastname" => $this->getLastname(),
			"street" => $this->getStreet(),
			"zip" => $this->getZip(),
			"city" => $this->getCity(),
			"country" => $this->getCountry(),
			"email" => $this->getEmail(),
			"birthday" => $date,
			"language" => $this->getLanguage()
		], $this->additional_data);

		ksort( $self );

		return $self;
	}
}