<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 03.04.2017
 * Time: 16:12
 */

namespace Payone\Config;


class ClearingType {

	const CREDITCARD            = 'cc';
	const ADVANCEPAYMENT        = 'vor';
	const DEBITPAYMENT          = 'elv';
	const ONLINEBANKTRANSFER    = 'sb';
	const WALLET                = 'wlt';
	const INVOICE               = 'rec';
	const CASHONDELIVERY        = 'cod';

	const FINANCING             = 'fnc';
	const BARZAHLEN             = 'csh';

	/**
	 * Only pre_authorizes may capture a payment
	 *
	 * @param $response
	 *
	 * @return bool
	 */
	public static function isCaptureAllowed( $response ) {

		if ( ! array_key_exists( 'clearingtype', $response ) ) {
			return false;
		}

		return in_array( $response['clearingtype'], [
			self::CREDITCARD,
			self::ADVANCEPAYMENT,
			self::CASHONDELIVERY,
			self::INVOICE,
		] );
	}

	/**
	 * After settlement only TX_CAPTURE and TX_PAID are allowed
	 * TODO: need validation from payone
	 * @param $response
	 * @return bool
	 */
	public static function isCallbackAllowedAfterSettlement( $response ) {

		if ( ! array_key_exists( 'txaction', $response ) ) {
			return false;
		}

		return in_array( $response['txaction'], [
			Globals::TX_CAPTURE,
			Globals::TX_PAID,
			Globals::TX_V_SETTLEMENT
		] );
	}
}