<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 15:57
 */

namespace Payone\Config;


class ShippingDataConfig {

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string
     */
    private $company;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $country;

    public function getFirstname() {

        return $this->firstname;
    }

    public function setFirstname( $firstname ) {

        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname() {

        return $this->lastname;
    }

    public function setLastname( $lastname ) {

        $this->lastname = $lastname;

        return $this;
    }

    public function getCompany() {

        return $this->company;
    }

    public function setCompany( $company ) {

        $this->company = $company;

        return $this;
    }

    public function getStreet() {

        return $this->street;
    }

    public function setStreet( $street ) {

        $this->street = $street;

        return $this;
    }

    public function getZip() {

        return $this->zip;
    }

    public function setZip( $zip ) {

        $this->zip = $zip;

        return $this;
    }

    public function getCity() {

        return $this->city;
    }

    public function setCity( $city ) {

        $this->city = $city;

        return $this;
    }

    public function getCountry() {

        return $this->country;
    }

    public function setCountry( $country ) {

        $this->country = $country;

        return $this;
    }


    public function __toArray() {

        $date = null;

        if ( ! $this->getLastname() ) {
            throw new \Exception( 'Lastname must not be empty!' );
        }

        if ( ! $this->getCountry() ) {
            throw new \Exception( 'Country must not be empty!' );
        }

        $self = [
            "shipping_firstname" => $this->getFirstname(),
            "shipping_lastname"  => $this->getLastname(),
            "shipping_company"   => $this->getCompany(),
            "shipping_street"    => $this->getStreet(),
            "shipping_zip"       => $this->getZip(),
            "shipping_city"      => $this->getCity(),
            "shipping_country"   => $this->getCountry(),
        ];

        ksort( $self );

        return $self;
    }
}
