<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 03.04.2017
 * Time: 17:33
 */

namespace Payone\Config;


class Globals {

	const PLUGIN_VERSION = '1.1';

	const DEFAULT_ENCODING  = 'UTF-8';
	const API_VERSION       = '3.8';

	const TEST_MODE = 'test';
	const LIVE_MODE = 'live';

	const IFRAME    = "https://secure.pay1.de/client-api/js/v1/payone_hosted_min.js";

	/**
	 *  The return value for a successful transaction request from payone
	 */
	const TRANSACTION_STATUS_OK = 'TSOK';

	const AUTHORIZATION     = 'authorization';
	const PRE_AUTHORIZATION = 'preauthorization';

	/**
	 * The URL of the Payone API
	 */
	const PAYONE_SERVER_API_URL = 'https://api.pay1.de/post-gateway/';

	/**
	 * different response types from request
	 */
	const RESPONSE_REDIRECT         = 'REDIRECT';
	const RESPONSE_APPROVED         = 'APPROVED';
	const RESPONSE_ERROR            = 'ERROR';
	const RESPONSE_PENDING          = 'PENDING';
	const RESPONSE_YES              = 'yes';
	const RESPONSE_NO               = 'no';
	const RESPONSE_AUTO             = 'auto';
	const RESPONSE_SETTLE_ACCOUNT   = 'settleaccount';

	/**
	 * Transaction Status values in txaction
	 */
	const TX_PAID           = 'paid';
	const TX_APPOINTED      = 'appointed';
	const TX_CAPTURE        = 'capture';
	const TX_UNDER_PAID     = 'underpaid';
	const TX_CANCEL         = 'cancelation';
	const TX_REFUND         = 'refund';
	const TX_DEBIT          = 'debit';
	const TX_REMINDER       = 'reminder';
	const TX_V_AUTH         = 'vauthorization';
	const TX_V_SETTLEMENT   = 'vsettlement';
	const TX_TRANSFER       = 'transfer';
	const TX_INVOICE        = 'invoice';
	const TX_FAILED         = 'failed';

	/**
	 * Invoicing
	 */
	const DELIVERY_BY_POST  = 'M';
	const DELIVERY_BY_MAIL  = 'P';
	const NO_DELIVERY       = 'N';

	/**
	 * Ecommercemode
	 */
	const MODE_INTERNET     = 'internet';
	const MODE_3DSECURE     = '3dsecure';
	const MODE_MOTO         = 'moto';

	/**
	 * Test Data
	 */
	const IBAN      = 'DE85123456782599100003';
	const BIC       = 'TESTTEST';
	const VISA      = '4111111111111111';
	const MASTER    = '5500000000000004';
	const MAESTRO   = '5000000000000009';

	const VISA_3D   = '4012001037141112';

}