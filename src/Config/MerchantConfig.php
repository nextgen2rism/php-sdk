<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 15:57
 */

namespace Payone\Config;


class MerchantConfig {

	/**
	 * the Account ID (aid)
	 * @var string
	 */
	private $account_id;

	/**
	 * The Merchant ID (mid)
	 * @var string
	 */
	private $merchant_id;

	/**
	 * The Portal ID (portalid)
	 * @var string
	 */
	private $portal_id;

	/**
	 * Mode can be either test or live and will be automatically set to test if not explicitly set to live
	 * @var string
	 */
	private $mode;

	/**
	 * MerchantConfig constructor.
	 *
	 * @param string $mode
	 */
	public function __construct( $mode ) {
		$this->setMode( $mode );
	}


	public function getAccountId() {
		return $this->account_id;
	}

	public function setAccountId( $account_id ) {
		$this->account_id = $account_id;

		return $this;
	}

	public function getMerchantId() {
		return $this->merchant_id;
	}

	public function setMerchantId( $merchant_id ) {
		$this->merchant_id = $merchant_id;

		return $this;
	}

	public function getPortalId() {
		return $this->portal_id;
	}

	public function setPortalId( $portal_id ) {
		$this->portal_id = $portal_id;

		return $this;
	}

	public function getMode() {
		return $this->mode;
	}

	public function setMode( $mode ) {
		$this->mode = $mode;

		if ( $this->mode !== Globals::LIVE_MODE ) {
			$this->mode = Globals::TEST_MODE;
		}

		return $this;
	}

	/**
	 * Magic method to receive the configuration data from this object as array (required by endpoint)
	 *
	 * @return array the array sorted by keys
	 */
	public function __toArray( ) {

		$self = [
			"aid" => $this->getAccountId(),
			"mid" => $this->getMerchantId(),
			"portalid" => $this->getPortalId(),
			"mode" => $this->getMode()
		];

		ksort( $self );

		return $self;
	}

}