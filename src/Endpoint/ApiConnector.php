<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 15:49
 */

namespace Payone\Endpoint;

use GuzzleHttp\Client;
use Payone\Config\Globals;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class ApiConnector {

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @var Client
	 */
	private $client;

	private $response_message;

	public function getResponseMessage() {
		return $this->response_message;
	}

	/**
	 * Connector constructor.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct( LoggerInterface $logger ) {
		$this->logger = $logger;
	}

	public function getClient() {

		if ( ! $this->client ) {
			$this->client = new Client();
		}

		return $this->client;
	}

	public function setClient( $client ) {
		$this->client = $client;
	}

	/**
	 * performing the POST request to the PAYONE platform
	 *
	 * @param array $request
	 *
	 * @throws \Exception
	 * @return array
	 */
	public function sendRequest( $request ) {

		$this->logger->info( sprintf( "Requesting %s with %s...", Globals::PAYONE_SERVER_API_URL, json_encode( $request ) ) );

		$stopwatch = new Stopwatch();
		$stopwatch->start( 'send_request' );

		if ( $response = $this->getClient()
			->request( 'POST', Globals::PAYONE_SERVER_API_URL, [ 'form_params' => $request ] ) ) {

			$return = $this->parseResponse( $response );
		} else {

			$this->logger->info( $stopwatch->stop( 'send_request' ) );

			throw new \Exception( 'Something went wrong during the HTTP request.' );
		}

		$this->logger->info( $stopwatch->stop( 'send_request' ) );

		return $return;
	}

	/**
	 * gets response string an puts its content into an array
	 *
	 * @param \Psr\Http\Message\ResponseInterface $response
	 *
	 * @throws \Exception
	 * @return array
	 */
	private function parseResponse( ResponseInterface $response ) {
		$responseArray = array();
		$explode       = explode( PHP_EOL, $response->getBody() );
		foreach ( $explode as $e ) {
			$keyValue = explode( "=", $e );
			if ( trim( $keyValue[0] ) != "" ) {
				if ( count( $keyValue ) == 2 ) {
					$responseArray[ $keyValue[0] ] = trim( $keyValue[1] );
				} else {
					$key = $keyValue[0];
					unset( $keyValue[0] );

					$value                 = implode( "=", $keyValue );
					$responseArray[ $key ] = $value;
				}
			}
		}

		if ( ! array_key_exists( 'status', $responseArray ) || $responseArray['status'] == "ERROR" ) {
			$this->response_message = $responseArray;
			throw new \Exception( sprintf( 'Payone returned an error: %s', json_encode( $responseArray ) ) );
		}

		return $responseArray;
	}
}