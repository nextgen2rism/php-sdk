<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 16:13
 */

namespace Payone\Payments;

use Payone\Config\ClearingType;
use Payone\Config\Globals;
use Payone\Requests\BasicPaymentRequest;
use Payone\Requests\ExtendedPaymentRequest;

/**
 *
 * Nachnahme
 *
 * Class CashOnDeliveryPayment
 * @package Payone\Payments
 */
class CashOnDeliveryPayment extends BasicPaymentRequest {

	private $shipping_provider;

	public function getShippingProvider() {
		return $this->shipping_provider;
	}

	public function setShippingProvider( $shipping_provider ) {
		$this->shipping_provider = $shipping_provider;
	}

	protected function sanity_check() {
		if ( ! $this->getShippingProvider() ) {
			$this->setShippingProvider( 'DHL' );
		}

		return true;
	}

	protected function getPaymentParameters() {
		return [
			'clearingtype'     => ClearingType::CASHONDELIVERY,
			'request'          => Globals::PRE_AUTHORIZATION,
			'shippingprovider' => $this->getShippingProvider(),
		];
	}

}