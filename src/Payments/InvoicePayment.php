<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 16:13
 */

namespace Payone\Payments;

use Payone\Config\ClearingType;
use Payone\Config\Globals;
use Payone\Requests\BasicPaymentRequest;
use Payone\Requests\ExtendedPaymentRequest;

/**
 *
 * Rechnung via
 *
 *  BSV BillSAFE Invoice
 *  KLV Klarna Invoice
 *  KLS Klarna Installment
 *
 * Class InvoicePayment
 * @package Payone\Payments
 */
class InvoicePayment extends BasicPaymentRequest {

	private $delivery_method;

	private $product_data = [];


	protected function sanity_check() {
		if ( ! $this->personal_data_config->getEmail() ) {
			return 'Email must be set to use with Invoice!';
		}

		return true;
	}

	protected function getPaymentParameters() {

		return array_merge(
			[
				'clearingtype' => ClearingType::INVOICE,
				'request' => Globals::PRE_AUTHORIZATION,
			],
			$this->getDeliveryMode(),
			$this->product_data
		);
	}

	private function getDeliveryMode() {
		return [
			'invoice_deliverymode' => $this->getDeliveryMethod()
		];
	}

	public function getDeliveryMethod() {
		return $this->delivery_method;
	}

	public function setDeliveryMethod( $delivery_method ) {
		$this->delivery_method = $delivery_method;
	}

	public function formatDate( \DateTime $dateTime ) {
		return $dateTime->format('ymd');
	}

	public function getEmptyItem() {
		return [
			'id' => 'The Id of the item',
			'pr' => 0,
			'no' => 0,
			'de' => 'The description of the item',
			'va' => 19,
			'sd' => $this->formatDate( new \DateTime() ),
			'ed' => $this->formatDate( new \DateTime() ),
		];
	}

	public function addItem( array $item ) {

		if ( ! $item ) {
			return $this;
		}

		if ( ! $this->product_data ) {
			$this->product_data = [];
		}

		$this->product_data[] = $item;

		return $this;
	}



}