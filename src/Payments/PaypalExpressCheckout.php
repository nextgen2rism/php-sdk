<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 15.01.2018
 * Time: 15:14
 */

namespace Payone\Payments;


use Payone\Config\Globals;
use Payone\Requests\BaseRequest;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Psr\Log\LoggerAwareTrait;

class PaypalExpressCheckout implements BaseRequest {

	use LoggerAwareTrait;

	/**
	 * @var string
	 */
	private $client_id;

	/**
	 * @var string
	 */
	private $client_secret;

	/**
	 * @var string
	 */
	private $mode;

	/**
	 * @var string
	 */
	private $response_message;

	/**
	 * @var string
	 */
	private $order_text;

	/**
	 * @var string
	 */
	private $success_url;

	/**
	 * @var string
	 */
	private $cancel_url;

	/**
	 * @var string
	 */
	private $error_url;

	/**
	 * Total amount (in smallest currency unit! e.g. cent)
	 *
	 * @var int
	 */
	private $amount;

	/**
	 * Currency (ISO 4217)
	 *
	 * @var string
	 */
	private $currency;

	public function getResponseMessage() {
		return $this->response_message;
	}

	private function getContext() {
		$apiContext = new ApiContext( new OAuthTokenCredential( $this->client_id, $this->client_secret ) );

		$apiContext->setConfig( array(
			"mode" => $this->mode === Globals::LIVE_MODE ? 'live' : 'sandbox'
		) );

		return $apiContext;
	}

	private function _getAmount() {
		$amount = new Amount();
		$amount
			->setCurrency( $this->currency )
			->setTotal( $this->amount / 100 );

		return $amount;
	}

	private function _getTransaction() {

		$item = new Item();
		$item
			->setQuantity( 1 )
			->setName( $this->order_text )
			->setPrice( $this->amount / 100 )
			->setCurrency( $this->currency );

		$item_list = new ItemList();
		$item_list->setItems( array( $item ) );


		$transaction = new Transaction();
		$transaction
			->setAmount( $this->_getAmount() )
			->setDescription( $this->order_text )
			->setItemList( $item_list );

		return $transaction;
	}

	private function _getUrls() {
		$urls = new RedirectUrls();
		$urls
			->setReturnUrl( $this->success_url )
			->setCancelUrl( $this->cancel_url );

		return $urls;
	}

	private function getPayment() {

		$payer = new Payer();
		$payer->setPaymentMethod( "paypal" );

		$payment = new Payment();
		$payment
			->setIntent( "authorize" )
			->setPayer( $payer )
			->setRedirectUrls( $this->_getUrls() )
			->setTransactions( array( $this->_getTransaction() ) );

		return $payment;
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	protected function sanity_check() {

		if ( ! $this->getCurrency() ) {
			$this->setCurrency( 'EUR' );
		}

		if ( ! is_int( $this->getAmount() ) ) {
			throw new \Exception( 'Amount has to be an integer and must be set to lowest unit e.g. cents' );
		}

		if ( $this->getAmount() == 0 ) {
			throw new \Exception( 'Amount may not be zero' );
		}

		if ( ! $this->getOrderText() ) {
			$this->setOrderText( uniqid() );
		}

		return true;
	}

	public function doRequest() {

		if ( ! $this->sanity_check() ) {
			return false;
		}

		$context = $this->getContext();

		$payment = $this->getPayment();

		try {

			$payment->create( $context );

		} catch ( \Exception $e ) {

			$this->logger->error( $e );

			return [
				'error_message' => $e->getMessage(),
				'status'        => Globals::RESPONSE_ERROR
			];
		}

		$response = [];

		foreach ( $payment->getLinks() as $link ) {
			if ( $link->getRel() == 'approval_url' ) {
				$response['redirecturl'] = $link->getHref();
				break;
			}
		}

		return array_merge( $payment->toArray(), $response, [
				'txid'   => $payment->getId(),
				'status' => Globals::RESPONSE_REDIRECT
			] );
	}

	public function getClientId() {
		return $this->client_id;
	}

	public function setClientId( $client_id ) {
		$this->client_id = $client_id;

		return $this;
	}

	public function getClientSecret() {
		return $this->client_secret;
	}

	public function setClientSecret( $client_secret ) {
		$this->client_secret = $client_secret;

		return $this;
	}

	public function getMode() {
		return $this->mode;
	}

	public function setMode( $mode ) {
		$this->mode = $mode;

		return $this;
	}

	public function getOrderText() {
		return $this->order_text;
	}

	public function setOrderText( $order_text ) {
		$this->order_text = $order_text;

		return $this;
	}

	public function getSuccessUrl() {
		return $this->success_url;
	}

	public function setSuccessUrl( $success_url ) {
		$this->success_url = $success_url;

		return $this;
	}

	public function getCancelUrl() {
		return $this->cancel_url;
	}

	public function setCancelUrl( $cancel_url ) {
		$this->cancel_url = $cancel_url;

		return $this;
	}

	public function getErrorUrl() {
		return $this->error_url;
	}

	public function setErrorUrl( $error_url ) {
		$this->error_url = $error_url;

		return $this;
	}

	public function getAmount() {
		return $this->amount;
	}

	public function setAmount( $amount ) {
		$this->amount = $amount;

		return $this;
	}

	public function getCurrency() {
		return $this->currency;
	}

	public function setCurrency( $currency ) {
		$this->currency = $currency;

		return $this;
	}


}