<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 16:13
 */

namespace Payone\Payments;

use Payone\Config\ClearingType;
use Payone\Config\Globals;
use Payone\Requests\BasicPaymentRequest;
use Payone\Requests\ExtendedPaymentRequest;

/**
 * Paypal
 *
 * Class PaypalPayment
 * @package Payone\Payments
 */
class PaypalPayment extends ExtendedPaymentRequest {

	protected function do_sanity_check() {
		return true;
	}

	protected function getExtendedPaymentParameters() {
		return [
			'clearingtype' => ClearingType::WALLET,
			'request'      => Globals::AUTHORIZATION,
			'wallettype'   => 'PPE',
		];
	}

}