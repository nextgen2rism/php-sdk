<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 16:13
 */

namespace Payone\Payments;

use Payone\Config\ClearingType;
use Payone\Config\Globals;
use Payone\Requests\BasicPaymentRequest;
use Payone\Requests\ExtendedPaymentRequest;

/**
 * Creditcard
 *
 * Class CreditcardPayment
 * @package Payone\Payments
 */
class CreditcardPayment extends ExtendedPaymentRequest {


	/**
	 * @var string
	 */
	private $pan;

	/**
	 * @var string
	 */
	private $mode;

	public function getPan() {
		return $this->pan;
	}

	public function setPan( $pan ) {
		$this->pan = $pan;
	}

	public function getMode() {
		return $this->mode;
	}

	public function setMode( $mode ) {
		$this->mode = $mode;
	}

	protected function do_sanity_check() {

		if ( ! $this->getPan() && ! $this->getCustomerid() ) {
			return 'Pseudocard PAN or CustomerID is missing!';
		}

		return true;
	}

	protected function getExtendedPaymentParameters() {
		$data = [
			'clearingtype' => ClearingType::CREDITCARD,
			'request'      => Globals::PRE_AUTHORIZATION
		];

		if ( $this->getMode() ) {
			$data['ecommercemode'] = $this->getMode();
		}

		if ( $this->getPan() ) {
			$data['pseudocardpan'] = $this->getPan();
		}

		return $data;
	}

}