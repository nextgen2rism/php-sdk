<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 16:13
 */

namespace Payone\Payments;

use Payone\Config\ClearingType;
use Payone\Config\Globals;
use Payone\Requests\BasicPaymentRequest;

/**
 * Vorkasse
 *
 * Class AdvancePayment
 * @package Payone\Payments
 */
class AdvancePayment extends BasicPaymentRequest {

	protected function sanity_check() {
		return true;
	}

	protected function getPaymentParameters() {
		return [
			'clearingtype' => ClearingType::ADVANCEPAYMENT,
			'request' => Globals::PRE_AUTHORIZATION
		];
	}

}