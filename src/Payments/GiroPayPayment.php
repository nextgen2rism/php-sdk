<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 16:13
 */

namespace Payone\Payments;

use Payone\Config\ClearingType;
use Payone\Config\Globals;
use Payone\Requests\BasicPaymentRequest;
use Payone\Requests\ExtendedPaymentRequest;

/**
 *
 * ELV with GiroPay (germany only)
 *
 * Class GiroPayPayment
 * @package Payone\Payments
 */
class GiroPayPayment extends BasicPaymentRequest {

	private $iban;
	private $bic;
	private $bankcountry;

	public function getIban() {
		return $this->iban;
	}

	public function setIban( $iban ) {
		$this->iban = $iban;

		return $this;
	}

	public function getBic() {
		return $this->bic;
	}

	public function setBic( $bic ) {
		$this->bic = $bic;

		return $this;
	}

	public function getBankcountry() {
		return $this->bankcountry;
	}

	public function setBankcountry( $bankcountry ) {
		$this->bankcountry = $bankcountry;

		return $this;
	}

	protected function sanity_check() {
		if ( ! $this->getIban() ) {
			return 'IBAN is missing!';
		}

		if ( ! $this->getBic() ) {
			return 'BIC is missing!';
		}

		if ( ! $this->getBankcountry() ) {
			$this->setBankcountry( 'DE' );
		}

		// GiroPay only in Germany
		if ( $this->getBankcountry() !== 'DE' ) {
			return 'GiroPay is only available in DE!';
		}

		// Remove Empty spaces in IBAN / Bic
		// uppercase transform IBAN / BIC
		$this->setIban( mb_strtoupper( preg_replace('/\s+/', '', $this->getIban() ) ) );
		$this->setBic( mb_strtoupper( preg_replace('/\s+/', '', $this->getBic() ) ) );

		return true;
	}

	protected function getPaymentParameters() {
		return [
			'clearingtype' => ClearingType::DEBITPAYMENT,
			'request' => Globals::AUTHORIZATION,
			'onlinebanktransfertype' => 'GPY',
			'bankcountry' => $this->getBankcountry(),
			'iban' => $this->getIban(),
			'bic' => $this->getBic()
		];
	}


}