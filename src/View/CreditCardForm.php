<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 04.04.2017
 * Time: 12:42
 */

namespace Payone\View;


use Payone\Config\ApiConfig;
use Payone\Config\Globals;
use Payone\Config\MerchantConfig;

class CreditCardForm {


	/**
	 * @var ApiConfig
	 */
	private $api_config;

	/**
	 * @var MerchantConfig
	 */
	private $merchant_config;

	/**
	 * CreditCardForm constructor.
	 *
	 * @param ApiConfig $api_config
	 * @param MerchantConfig $merchant_config
	 */
	public function __construct( ApiConfig $api_config, MerchantConfig $merchant_config ) {
		$this->api_config      = $api_config;
		$this->merchant_config = $merchant_config;
	}

	private function getConfig() {
		$base_config = [
			'request' =>  'creditcardcheck',
			'responsetype' => 'JSON',
			'storecarddata' => 'yes'
		];

		$base_config['encoding'] = $this->api_config->getEncoding();

		$config = array_merge ( $base_config, $this->merchant_config->__toArray() );

		ksort( $config );

		return $config;
	}

	private function generateHash( $config ) {
		return hash_hmac('sha384', implode('', $config ), $this->api_config->getPortalSecret() );
	}

	public function getJavascriptConfig() {

		$config = $this->getConfig();
		$config['hash'] = $this->generateHash( $config );

		return $config;
	}

	public function generateJavascript( $options = array(), $namespace = 'window', $preload = true ) {

		$config = $this->getJavascriptConfig();

		$request = json_encode($config);

		$config = json_encode($options);

		$config = str_replace('"Payone.ClientApi.Language.de"', 'Payone.ClientApi.Language.de', $config);

		$data = <<<EOT
			var request, config;
			
			config = $config;
			request = $request;
			
			var iframes = new Payone.ClientApi.HostedIFrames(config, request);
            iframes.setCardType("V");

		    document.getElementById('cardtype').onchange = function () {
		       iframes.setCardType(this.value); 
		    };
		    
			$namespace.check = function() {
				
				${namespace}.onclick();
								
				if ( iframes.isComplete() ) {
                    iframes.creditCardCheck('${namespace}Callback');
		        } else {
		            ${namespace}.onerror(  );
		        }
			}
			
			window.${namespace}Callback = function(response) {
				
				${namespace}.beforesubmit();
				
				if (response.status === "VALID") {
                    document.getElementById("pseudocardpan").value = response.pseudocardpan;
                    document.getElementById("truncatedcardpan").value = response.truncatedcardpan;
                    document.getElementById('cardexpiredate').value = response.cardexpiredate;
                    document.paymentform.submit();
                } else {
                    ${namespace}.onerror( response );
                }
			};
EOT;


		$script = $data;

		if ( $preload ) {
			$script =   '<script type="text/javascript" src="'. $this->getIframe() . '"></script>';
			$script .=  '<script language="javascript">' . $data . '</script>';
		}


		return $script;
	}

	public function getIframe() {
		return Globals::IFRAME;
	}

}