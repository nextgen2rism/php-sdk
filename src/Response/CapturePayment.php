<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 04.04.2017
 * Time: 16:50
 */

namespace Payone\Response;



use Payone\Config\ApiConfig;
use Payone\Config\MerchantConfig;
use Payone\Endpoint\ApiConnector;
use Psr\Log\LoggerInterface;

class CapturePayment  {

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @var int
	 */
	private $sequencenumber;

	/**
	 * @var string
	 */
	private $txid;

	/**
	 * @var ApiConfig
	 */
	private $api_config;

	/**
	 * @var MerchantConfig
	 */
	private $merchant_config;
	/**
	 * @var string
	 */
	private $currency;

	/**
	 * @var int
	 */
	private $amount;

	private $response_message;

	/**
	 * CapturePayment constructor.
	 *
	 * @param LoggerInterface $logger
	 * @param ApiConfig $api_config
	 * @param MerchantConfig $merchant_config
	 */
	public function __construct( LoggerInterface $logger, ApiConfig $api_config, MerchantConfig $merchant_config ) {
		$this->logger          = $logger;
		$this->api_config      = $api_config;
		$this->merchant_config = $merchant_config;
	}

	public function setSequencenumber( $sequencenumber ) {
		$this->sequencenumber = $sequencenumber;

		return $this;
	}

	public function setTxid( $txid ) {
		$this->txid = $txid;

		return $this;
	}

	public function getSequencenumber() {
		return $this->sequencenumber;
	}

	public function setCurrency( $currency ) {
		$this->currency = $currency;

		return $this;
	}

	public function setAmount( $amount ) {
		$this->amount = $amount;

		return $this;
	}

	public function getResponseMessage() {
		return $this->response_message;
	}



	private function getRequestConfig() {
		$this->sequencenumber += 1;

		if ( ! $this->currency ) {
			$this->currency = 'EUR';
		}

		$config = [
			'request' => 'capture',
			'txid' => $this->txid,
			'currency' => $this->currency,
			'sequencenumber' => $this->sequencenumber,
			'amount' => $this->amount
		];

		return $config;
	}


	public function doRequest() {

		$request = array_merge(
			$this->getRequestConfig(),
			$this->api_config->__toArray(),
			$this->merchant_config->__toArray()
		);

		$connector = new ApiConnector( $this->logger );

		$response = false;

		try {

			$response = $connector->sendRequest( $request);

		} catch( \Exception $e ) {

			$this->logger->error( $e );
		}

		$this->response_message = $connector->getResponseMessage();

		return $response;
	}
}