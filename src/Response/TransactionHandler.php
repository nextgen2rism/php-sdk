<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 03.04.2017
 * Time: 17:25
 */

namespace Payone\Response;


use Payone\Config\ApiConfig;
use Payone\Config\Globals;
use Psr\Log\LoggerInterface;

class TransactionHandler {

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @var ApiConfig
	 */
	private $api_config;

	/**
	 * @var array
	 */
	private $response;

	/**
	 * TransactionStatus constructor.
	 *
	 * @param LoggerInterface $logger
	 * @param ApiConfig $api_config
	 */
	public function __construct( LoggerInterface $logger, ApiConfig $api_config ) {
		$this->logger     = $logger;
		$this->api_config = $api_config;
	}

	/**
	 * Verifies the request data
	 * @param array $request_data
	 * @return bool Returns false if the transaction is fraudulent or erroneous
	 */
	public function verifyRequest( $request_data ) {

		if ( ! array_key_exists('key', $request_data) ) {
			return false;
		}

		if ( $request_data['key'] != $this->api_config->getSecretHash() ) {
			return false;
		}

		return true;
	}

	/**
	 * Method has to be called by the controller
	 *
	 * it will evaluate the payload and set the response if applicable
	 *
	 * @see Server API Description, Section 4.2
	 *
	 * @param $request_data
	 *
	 * @return bool Returns false if the transaction is fraudulent or erroneous
	 */
	public function handleTransaction( $request_data ) {

		$this->response = false;

		if ( ! $this->verifyRequest( $request_data ) ) {
			return false;
		}

		$this->response = $request_data;

		echo Globals::TRANSACTION_STATUS_OK;

		return true;
	}

	/**
	 * The response or false if not applicable
	 *
	 * @return array|bool
	 */
	public function getResponse() {
		return $this->response;
	}

}