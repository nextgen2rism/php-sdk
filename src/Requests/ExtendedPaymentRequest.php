<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 04.04.2017
 * Time: 08:53
 */

namespace Payone\Requests;


abstract class ExtendedPaymentRequest extends BasicPaymentRequest {
	/**
	 * @var string
	 */
	private $order_text;

	/**
	 * @var string
	 */
	private $success_url;

	/**
	 * @var string
	 */
	private $cancel_url;

	/**
	 * @var string
	 */
	private $error_url;

	public function getOrderText() {
		return $this->order_text;
	}

	public function setOrderText( $order_text ) {
		$this->order_text = $order_text;

		return $this;
	}

	public function getSuccessUrl() {
		return $this->success_url;
	}

	public function setSuccessUrl( $success_url ) {
		$this->success_url = $success_url;

		return $this;
	}

	public function getCancelUrl() {
		return $this->cancel_url;
	}

	public function setCancelUrl( $cancel_url ) {
		$this->cancel_url = $cancel_url;

		return $this;
	}

	public function getErrorUrl() {
		return $this->error_url;
	}

	public function setErrorUrl( $error_url ) {
		$this->error_url = $error_url;

		return $this;
	}

	protected function sanity_check() {

		if ( ! $this->getSuccessUrl() && ! $this->getCustomerid() ) {
			return 'Success URL or CustomerID is missing';
		}

		if ( ! $this->getCancelUrl() && ! $this->getCustomerid() ) {
			return 'Cancel URL or CustomerID is Missing';
		}

		if ( ! $this->getErrorUrl() ) {
			$this->setErrorUrl( $this->getCancelUrl() );
		}

		if ( ! $this->getOrderText() ) {
			$this->setOrderText( $this->getReference() );
		}

		return $this->do_sanity_check();
	}

	/**
	 * provides a simple way to verify all required parameters before submitting the request
	 * @return boolean
	 */
	protected abstract function do_sanity_check();

	/**
	 * must return the required payment parameters corresponding to the selected paymethod
	 *
	 * @return array
	 */
	protected abstract function getExtendedPaymentParameters();

	protected function getPaymentParameters() {
		$data = [
			'narrative_text' => $this->getOrderText(),
		];

		if( $this->getSuccessUrl() ) {
			$data['successurl'] = $this->getSuccessUrl();
		}

		if( $this->getCancelUrl() ) {
			$data['backurl'] = $this->getCancelUrl();
		}

		if( $this->getErrorUrl() ) {
			$data['errorurl'] = $this->getErrorUrl();
		}

		return array_merge( $this->getExtendedPaymentParameters(), $data);
	}


}