<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 04.04.2017
 * Time: 08:53
 */

namespace Payone\Requests;


abstract class BasicPaymentRequest extends AbstractRequest {

	/**
	 * Merchant reference number for the payment process.
	 * @var string
	 */
	private $reference;

	/**
	 * Total amount (in smallest currency unit! e.g. cent)
	 *
	 * @var int
	 */
	private $amount;

	/**
	 * Currency (ISO 4217)
	 *
	 * @var string
	 */
	private $currency;

	/**
	 * The Userid given by the shop, allow follow-up payments
	 *
	 * @var string
	 */
	private $customerid;

	/**
	 * Debtor ID (PAYONE)
	 *
	 * The Userid from the Payone Response to allow follow-up payments
	 *
	 * @var string
	 */
	private $userid;

    protected function isShippingDataRequired() {
        return false;
    }

    public function getReference() {
		return $this->reference;
	}

	public function setReference( $reference ) {
		$this->reference = $reference;

		return $this;
	}

	public function getAmount() {
		return $this->amount;
	}

	public function setAmount( $amount ) {
		$this->amount = $amount;

		return $this;
	}

	public function getCurrency() {
		return $this->currency;
	}

	public function setCurrency( $currency ) {
		$this->currency = $currency;

		return $this;
	}

	public function getCustomerid() {
		return $this->customerid;
	}

	public function setCustomerid( $customerid ) {
		$this->customerid = $customerid;

		return $this;
	}

	public function getUserid() {
		return $this->userid;
	}

	public function setUserid( $userid ) {
		$this->userid = $userid;

		return $this;
	}


	/**
	 * provides a simple way to verify all required parameters before submitting the request
	 * @return boolean
	 */
	protected abstract function sanity_check();

	/**
	 * must return the required payment parameters corresponding to the selected paymethod
	 *
	 * @return array
	 */
	protected abstract function getPaymentParameters();

	/**
	 * evaluates the required fields and sets appropriate default values
	 * @throws \Exception
	 */
	protected function basic_sanity_check() {

		if ( ! $this->getCurrency() ) {
			$this->setCurrency('EUR');
		}

		if ( ! is_int( $this->getAmount() ) ) {
			throw new \Exception('Amount has to be an integer and must be set to lowest unit e.g. cents');
		}

		if ( $this->getAmount() == 0 ) {
			throw new \Exception('Amount may not be zero');
		}

		if ( ! $this->getReference() ) {
			$this->setReference( uniqid() );
		}

		$check = $this->sanity_check();

		if ( is_string( $check ) ) {
			throw new \Exception( $check );
		}

		return $check;
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	protected function getRequestConfig() {

		if ( ! $this->basic_sanity_check() ) {
			throw new \Exception('Missing Values in Configuration, please check your config!');
		}

		$sorted_data = array_merge( $this->getPaymentParameters(), [
			'reference' => $this->getReference(),
			'amount' => $this->getAmount(),
			'currency' => $this->getCurrency(),
		]);

		if ( $this->getCustomerid() ) {
			$sorted_data['customerid'] = $this->getCustomerid();
		}

		if ( $this->getUserid() ) {
			$sorted_data['userid'] = $this->getUserid();
		}

		ksort($sorted_data);

		return $sorted_data;
	}



}
