<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 15.01.2018
 * Time: 15:12
 */

namespace Payone\Requests;


interface BaseRequest {

	public function getResponseMessage();
	public function doRequest();

}
