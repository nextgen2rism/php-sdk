<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 16:13
 */

namespace Payone\Requests;


use Payone\Config\ApiConfig;
use Payone\Config\MerchantConfig;
use Payone\Config\PersonalDataConfig;
use Payone\Config\ShippingDataConfig;
use Payone\Endpoint\ApiConnector;
use Psr\Log\LoggerInterface;

abstract class AbstractRequest implements BaseRequest {

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ApiConfig
     */
    private $api_config;

    /**
     * @var MerchantConfig
     */
    private $merchant_config;

    /**
     * @var PersonalDataConfig
     */
    protected $personal_data_config;

    /**
     * @var ShippingDataConfig
     */
    protected $shipping_data_config;

    /**
     * @var array
     */
    protected $additional_data;

    private $response_message;

    /**
     * AbstractRequest constructor.
     *
     * @param LoggerInterface $logger
     * @param ApiConfig $api_config
     * @param MerchantConfig $merchant_config
     * @param PersonalDataConfig $personal_data_config
     * @param ShippingDataConfig $shipping_data_config
     */
    public function __construct( LoggerInterface $logger, ApiConfig $api_config, MerchantConfig $merchant_config, PersonalDataConfig $personal_data_config, $shipping_data_config = null ) {

        $this->logger               = $logger;
        $this->api_config           = $api_config;
        $this->merchant_config      = $merchant_config;
        $this->personal_data_config = $personal_data_config;
        $this->shipping_data_config = $shipping_data_config;
        $this->additional_data      = array();
    }

    protected abstract function getRequestConfig();

    protected abstract function isShippingDataRequired();

    public function setAdditionalData( $additional_data ) {

        $this->additional_data = $additional_data;
    }

    public function getResponseMessage() {

        return $this->response_message;
    }

    public function doRequest() {

        $request = array_merge( $this->getRequestConfig(),
            $this->api_config->__toArray(),
            $this->merchant_config->__toArray(),
            $this->personal_data_config->__toArray(),
            $this->additional_data );

        if ( $this->isShippingDataRequired() && $this->shipping_data_config ) {
            $request = array_merge( $request, $this->shipping_data_config->__toArray() );
        }

        $connector = new ApiConnector( $this->logger );

        try {

            return $connector->sendRequest( $request );

        } catch ( \Exception $e ) {
            $this->logger->error( $e );
        }

        $this->response_message = $connector->getResponseMessage();

        return false;
    }

}
