<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 16:26
 */


use PHPUnit\Framework\TestCase;
use Payone\Endpoint\ApiConnector;

/**
 * @covers \Payone\Endpoint\ApiConnector
 * Class ApiConnectorTest
 */
class ApiConnectorTest extends TestCase {

	private $stubbed_logger;

	protected function setUp() {
		parent::setUp();

		$this->stubbed_logger = $this->createMock(\Psr\Log\LoggerInterface::class);

		$this->stubbed_logger->method('info')->willReturn('null');


	}


	public function test_FailedRequest_WillThrowException() {

		$failed_request = null;

		$stubbed_client = $this->createMock(\GuzzleHttp\Client::class );
		$stubbed_client->method('request')->willReturn($failed_request);

		$connector = new ApiConnector( $this->stubbed_logger );
		$connector->setClient( $stubbed_client );

		$this->expectException(Exception::class);

		$connector->sendRequest( null );
	}

	public function test_InvalidRequest_WillThrowException() {

		$expected_response = '';

		$stubbed_response = $this->createMock(\Psr\Http\Message\ResponseInterface::class );
		$stubbed_response->method('getBody')->willReturn( $expected_response );

		$stubbed_client = $this->createMock(\GuzzleHttp\Client::class );
		$stubbed_client->method('request')->willReturn($stubbed_response);

		$connector = new ApiConnector( $this->stubbed_logger );
		$connector->setClient( $stubbed_client );

		$this->expectException(Exception::class);

		$connector->sendRequest( null );
	}

	public function test_SuccessfulRequest_WillCreateArray() {

		$expected_response = implode(PHP_EOL, [
			'status=success',
			'key=value',
			'key'
		]);

		$stubbed_response = $this->createMock(\Psr\Http\Message\ResponseInterface::class );
		$stubbed_response->method('getBody')->willReturn( $expected_response );

		$stubbed_client = $this->createMock(\GuzzleHttp\Client::class );
		$stubbed_client->method('request')->willReturn($stubbed_response);

		$connector = new ApiConnector( $this->stubbed_logger );
		$connector->setClient( $stubbed_client );

		$response = $connector->sendRequest( null );

		$this->assertEquals( true, is_array($response));
	}


	public function test_HttpClientIsNull_WillCreateNewClient() {

		$client = null;

		$connector = new ApiConnector( $this->stubbed_logger );
		$connector->setClient($client);

		$this->assertNotEquals( $client, $connector->getClient() );
	}

}
