<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 16:26
 */

use Payone\Config\ApiConfig;
use PHPUnit\Framework\TestCase;

/**
 * @covers Payone\Config\ApiConfig
 * Class ApiConfigTest
 */
class ApiConfigTest extends TestCase {

	/**
	 * @var ApiConfig
	 */
	private $apiconfig;

	public function setUp() {
		$this->apiconfig = new ApiConfig('');
	}

	/**
	 * @covers Payone\Config\ApiConfig::__toArray()
	 */
	public function test_PortalSecretIsRequiredForToArrayMethod() {
		$portalSecretEmpty = '';
		$toArrayShouldReturnFalse = false;

		$this->apiconfig->setPortalSecret($portalSecretEmpty);

		$this->assertEquals($toArrayShouldReturnFalse, $this->apiconfig->__toArray() );
	}

	/**
	 * @covers Payone\Config\ApiConfig::__toArray()
	 */
	public function test_ToArrayMethodContainsRequiredParameterKeys() {
		$required_keys = [
			'key', 'encoding', 'api_version'
		];

		$empty_array = [];

		$portalSecret = 'this_is_my_secret';

		$this->apiconfig->setPortalSecret($portalSecret);

		$result = array_keys($this->apiconfig->__toArray());


		$this->assertEquals( $empty_array,  array_diff( $result, $required_keys ) );
	}

	/**
	 * @covers Payone\Config\ApiConfig::__toArray()
	 */
	public function test_ToArrayKeysAreSorted() {
		$required_keys = [
			'key', 'encoding', 'api_version'
		];

		$portalSecret = 'this_is_my_secret';

		$this->apiconfig->setPortalSecret($portalSecret);

		$result = array_keys($this->apiconfig->__toArray());

		sort( $required_keys );

		$this->assertEquals( $required_keys,  $result );
	}

	/**
	 * @covers Payone\Config\ApiConfig::__toArray()
	 */
	public function test_SecretKeyMustBeMD5Hashed() {
		$portalSecret = 'this_is_my_secret';
		$portalSecretMD5 = md5($portalSecret);

		$this->apiconfig->setPortalSecret($portalSecret);

		$result = $this->apiconfig->__toArray();

		$this->assertEquals($portalSecretMD5, $result['key'] );
	}

	/**
	 * @covers Payone\Config\ApiConfig::getEncoding()
	 * @covers Payone\Config\ApiConfig::setEncoding()
	 */
	public function test_DefaultEncodingMustBeSameAsConstant() {
		$defaultEncoding = \Payone\Config\Globals::DEFAULT_ENCODING;
		$this->apiconfig->setEncoding('');

		$this->assertEquals($defaultEncoding, $this->apiconfig->getEncoding() );
	}

	/**
	 * @covers Payone\Config\ApiConfig::getApiVersion()
	 * @covers Payone\Config\ApiConfig::setApiVersion()
	 */
	public function test_DefaultApiVersionMustBeSameAsConstant() {
		$default_api_version = \Payone\Config\Globals::API_VERSION;

		$this->apiconfig->setApiVersion('');

		$this->assertEquals($default_api_version, $this->apiconfig->getApiVersion() );
	}

}
