<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 16:26
 */

use Payone\Config\PersonalDataConfig;
use PHPUnit\Framework\TestCase;
use Payone\Config\Globals;

/**
 * @covers \Payone\Config\PersonalDataConfig
 * Class PersonalDataConfigTest
 */
class PersonalDataConfigTest extends TestCase {


	public function test_LastnameIsEmpty_WillThrowException() {
		$empty = '';

		$personal_data = new PersonalDataConfig();

		$personal_data->setLastname($empty);

		$this->expectException(Exception::class);

		$personal_data->__toArray();
	}

	public function test_LastnameIsNull_WillThrowException() {
		$null = null;

		$personal_data = new PersonalDataConfig();

		$personal_data->setLastname($null);

		$this->expectException(Exception::class);

		$personal_data->__toArray();
	}

	public function test_CountryIsEmpty_WillThrowException() {
		$empty = '';
		$not_empty = 'not_empty';

		$personal_data = new PersonalDataConfig();
		$personal_data->setLastname($not_empty)
		              ->setCountry($empty);

		$this->expectException(Exception::class);

		$personal_data->__toArray();

	}

	public function test_CountryIsNull_WillThrowException() {
		$null = null;
		$not_empty = 'not_empty';

		$personal_data = new PersonalDataConfig();
		$personal_data->setLastname($not_empty)
		              ->setCountry($null);

		$this->expectException(Exception::class);

		$personal_data->__toArray();
	}

	public function test_BirthdayIsSet_FormatWillBeSetToYMD() {

		$date = new DateTime();
		$date->setDate(1981, 8, 4);

		$expected = '19810804';

		$personal_data = new PersonalDataConfig();

		$personal_data->setCountry('country')
		              ->setLastname('lastname')
		              ->setBirthday( $date );

		$result = $personal_data->__toArray();

		$this->assertEquals( $expected, $result['birthday'] );
	}

	public function test_Config_WillBeSortedByKeys() {
		$personal_data = new PersonalDataConfig();
		$personal_data->setCountry('country')
		              ->setLastname('lastname');

		$sorted_keys = [
			"salutation",
			"firstname",
			"lastname",
			"street",
			"zip",
			"city",
			"country",
			"email",
			"birthday",
			"language"
		];

		sort( $sorted_keys );

		$sorted_key_set = array_keys( $personal_data->__toArray() );

		$this->assertEquals( $sorted_keys, $sorted_key_set );
	}

	public function test_Config_WillContainRequiredKeySet() {
		$personal_data = new PersonalDataConfig();
		$personal_data->setCountry('country')
		              ->setLastname('lastname');

		$required_keys = [
			"salutation",
			"firstname",
			"lastname",
			"street",
			"zip",
			"city",
			"country",
			"email",
			"birthday",
			"language"
		];

		$empty_array = [];

		$key_set = array_keys( $personal_data->__toArray() );

		$this->assertEquals( $empty_array, array_diff( $required_keys, $key_set) );

	}


	public function test_FluidSetters_WillSetValuesAccordingly() {

		$date = new DateTime();
		$date->setDate(1981, 8, 4);

		$expected = '19810804';


		$expected_result = array(
			"salutation" => "Mr.",
			"firstname" => "Henry",
			"lastname" => "Tudor",
			"street" => "Royal Street 1",
			"zip" => "24118",
			"city" => "Kiel",
			"country" => "DE",
			"email" => "henry.viii@tudor.gov.uk",
			"birthday" => $expected,
			"language" => "de"
		);

		ksort($expected_result);

		$personal_data = new PersonalDataConfig();

		$personal_data->setSalutation( $expected_result['salutation'] )
			->setFirstname( $expected_result['firstname'] )
			->setLastname( $expected_result['lastname'] )
			->setStreet( $expected_result['street'] )
			->setZip( $expected_result['zip'])
			->setCountry( $expected_result['country'])
			->setEmail( $expected_result['email'])
			->setBirthday( $date )
			->setCity( $expected_result['city'])
			->setLanguage( $expected_result['language'] );


		$this->assertEquals( $expected_result, $personal_data->__toArray() );
	}



}
