<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 28.03.2017
 * Time: 16:26
 */

use Payone\Config\MerchantConfig;
use PHPUnit\Framework\TestCase;
use Payone\Config\Globals;

/**
 * @covers \Payone\Config\MerchantConfig
 * Class MerchantConfigTest
 */
class MerchantConfigTest extends TestCase {


	public function test_DefaultModeNotSet_WillBeSetToTest() {
		$config_without_mode_defined = new MerchantConfig('');

		$test_mode = Payone\Config\Globals::TEST_MODE;

		$this->assertEquals( $test_mode, $config_without_mode_defined->getMode() );
	}

	public function test_ModeNotSetToLiveOrTest_WillBeSetToTest() {

		$config_with_wrong_mode = new MerchantConfig( 'wrong_mode' );

		$test_mode = Payone\Config\Globals::TEST_MODE;

		$this->assertEquals( $test_mode, $config_with_wrong_mode->getMode() );
	}

	public function test_Config_WillContainRequiredKeySet() {
		$config = new MerchantConfig('' );

		$required_keys = [
			'aid', 'mid', 'portalid', 'mode'
		];

		$empty_array = [];

		$key_set = array_keys( $config->__toArray() );

		$this->assertEquals( $empty_array, array_diff( $required_keys, $key_set) );
	}

	public function test_Config_WillBeSortedByKeys() {
		$config = new MerchantConfig('' );

		$sorted_keys = [
			'aid', 'mid', 'portalid', 'mode'
		];

		sort( $sorted_keys );

		$sorted_key_set = array_keys( $config->__toArray() );

		$this->assertEquals( $sorted_keys, $sorted_key_set );
	}

	public function test_FluidSetters_WillSetValuesAccordingly() {

		$portalid = 'portal_id';
		$mode = Globals::LIVE_MODE;
		$aid = 'aid';
		$mid = 'mid';

		$expected_result = compact('aid', 'mid', 'mode', 'portalid');

		ksort($expected_result);

		$config = new MerchantConfig( $mode );

		$config->setPortalId( $portalid )
		       ->setAccountId( $aid )
		       ->setMerchantId( $mid );

		$this->assertEquals( $expected_result, $config->__toArray() );
	}

}
