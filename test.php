<?php
/**
 * Created by PhpStorm.
 * User: b2wsra
 * Date: 04.04.2017
 * Time: 13:03
 */

include_once('vendor/autoload.php');

$dotenv = new Dotenv\Dotenv( __DIR__ );
$dotenv->load();

$test_data = [
	'mid' => getenv('MID'),
	'aid' => getenv('AID'),
	'portalid' => getenv('PORTALID'),
	'secret' => getenv('SECRET')
];

$personalData = array(
	"salutation" => "Herr",
	"title" => "Dr.",
	"firstname" => "Paul",
	"lastname" => "Neverpayer",
	"street" => "Fraunhoferstraße 2-4",
	"addressaddition" => "EG",
	"zip" => "24118",
	"city" => "Kiel",
	"country" => "DE",
	"email" => "paul.neverpayer@payone.de",
	"telephonenumber" => "043125968500",
	"birthday" => "19700204",
	"language" => "de",
	"gender" => "m",
	"ip" => "8.8.8.8"
);

$conf = new \Payone\Config\ApiConfig($test_data['secret']);

$merchant = new \Payone\Config\MerchantConfig(\Payone\Config\Globals::TEST_MODE);

$merchant->setMerchantId($test_data['mid'])
	->setAccountId($test_data['aid'])
	->setPortalId($test_data['portalid']);

$pdata = new \Payone\Config\PersonalDataConfig();

$pdata->setLastname($personalData['lastname'])
	->setCountry($personalData['country'])
	->setLanguage('de')
	->setAdditionalData( $personalData );

$logger = new \Monolog\Logger('test');

$payment = new \Payone\Payments\AdvancePayment($logger, $conf, $merchant, $pdata);

//$payment->setErrorUrl('http://dev.test.de');
//$payment->setCancelUrl('http://dev.test.de');
//$payment->setSuccessUrl('http://dev.test.de');
//$payment->setOrderText('asasd');
$payment->setAmount(1000);
//$payment->setIban('DE85123456782599100003');
//$payment->setBic('TESTTEST');

dump( $payment->doRequest() );

